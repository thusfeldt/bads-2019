
# Installing the `algs4` Java library 

This pages is about the Java library. For `python3`, you can find the installation instructions directly in the repository at https://github.itu.dk/algorithms/AlgorithmsInPython.

## 1. java 

Check that you have a java development kit (jdk) installed on your computer at all. Type the following command in a terminal:
```
javac -version
```
The answer should be something like `javac 1.8.0_144` (only the `1.8` part is important.) If so, proceed to section 2.

Otherwise, you don’t have Java on your machine, or your shell can’t find it. 

You can download Java for free from  https://java.com . Just take the recommended version (currenlty 8).

If you are positive that `javac` is installed but your shell can’t find it, then you have to make sure its location is referenced to in the `PATH` environment variable. The link
http://www.ntu.edu.sg/home/ehchua/programming/howto/Environment_Variables.html
 explains how to set your Path variable through the cmd on windows,
but it can also be done through regular system settings on, by navigating to (example is from windows 10):

"Control Panel ⇒ System and Security ⇒ System ⇒ Advanced System Settings ⇒ Advanced ⇒ Environment Variables..."

Then edit the "Path" variable, this will open a new window, where you can add the path to your Java.exe and Javac.exe files, which should be located in the bin folder, of the Java program folder.

## 2. algs4

Download the library as `algs4.jar` from 
https://algs4.cs.princeton.edu/code/algs4.jar
some directory, say `/my/sample/directory` such that the downloaded file has the fully qualified name 
`/my/sample/directory/algs4.jar` .

For Windows, try
```
> java -cp C:\my\sample\directory\algs4.jar edu.princeton.cs.algs4.Date
```
For Linux and MacOS
```
> java -cp /my/sample/directory/algs4.jar edu.princeton.cs.algs4.Date
```
These commands should produce a few lines of dates.

## 3. including algs4.jar in the CLASSPATH environment variable

It remains to inform your system that the classes in `/my/sample/directory/algs4.jar` should always be included when you run java. This is done by adding the file to the `CLASSPATH` environment variable. 

Setting the CLASSPATH is highly system dependent, the instructions below should work for most of you. More details can be found many places on the web, a good introduction is at http://www.ntu.edu.sg/home/ehchua/programming/howto/Environment_Variables.html .


#### Setting the CLASSPATH in Unix-like systems (MacOS X, Linux)

See if any of `~/.bash_profile`, `~/.bash_login`, or `~/.profile` already exist on your machine. If not, create `~/.bash_profile`. Add the line
```
export CLASSPATH=$CLASSPATH:~/algs4.jar
```

Now the following should work:
```
> java edu.princeton.cs.algs4.Date
```

#### Setting the CLASSPATH in Windows

On Windows 10, go to  "Control Panel ⇒ System and Security ⇒ System ⇒ Advanced System Settings ⇒ Advanced ⇒ Environment Variables..."
and add the classpath to the "CLASSPATH" variable.

Now the following should work:
```
> java edu.princeton.cs.algs4.Date
```

## Alternative approach: Java extensions library

A different approach is to download the libarary directly to the directory where Java will look for extensions, the “Java Extension Directory”. Depending on the system, this can be found at 

* `/Users/username/Library/Java/Extensions/` (on Mac OS X), 
* `/usr/java/packages/lib/ext/` (on Linux)
* `C:\Program Files\Java\jre1.8.0_191\lib\ext` (your Java version may differ) (on Windows)

This avoids having to set the CLASSPATH (which is good); on the other hand you might prefer to have algs4 installed in a directory for this current course instead.

## Instructions on the Princeton Booksite

The course booksite at Princeton, for their course, includes detailed instructions of installing the package for integrated development environments DrJava and IntelliJ. We are _not_ using those environments, so there is no reason to follow those instructions. (In fact, we _discourage_ using an IDE in this course.) If you _insist_ on using an IDE, you can do that, but you're on your own.



