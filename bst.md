The bst exercise on Kattis
--------------------------

## First attempt

The straightforward solution, sometimes called the *Nike approach*, is to “just do it.”
Implement a BST and simulate. 
Here's the code in Python: 

``` python
from sys import stdin

class Node:
    def __init__(self, key):
        self.left, self.right, self.key = None, None, key

    def insert(self, key):
        if self.key == None: # special case: first insert
            self.key = key
            return 0
        if key < self.key:
            if not self.left:
                self.left = Node(key)
                return 1
            else: return 1 + self.left.insert(key)
        if key > self.key:
            if not self.right:
                self.right = Node(key)
                return 1
            else: return 1 + self.right.insert(key)
        assert False # key is never already present in this exercise

T = Node(None)
n = int(input())
ct=0
for line in stdin:
    ct += T.insert(int(line))
    print (ct)
```

## Discussion

First: this is correct. Cause for triumph – if you understand this, you understand a lot. (Recursion, for instance.)

Discussion: what is the running time? 
* What is our *cost model*?
  - Let's count key comparisons
  - Would also make sense to count recursive calls, or additions, or accesses to fields of the Node object
  - Would not make sense to count array accesses
* Each insert takes time linear in the depth of the tree
* Total time in terms of $N$ is linearithmic in the best case, quadratic  in the worst case

Concepts used so far: recursion, binary search tree, cost model, running time, best case, worst case, keys. This is a *lot* of stuff.

Discipline: Try to use “key” for the thing inserted into the BST (rather “guy” or “thing”). In particular, try to avoid using “value”. BSTs are symbol tables, which address values by keys, and these concepts are *different*. In the context of this particular exercise “the integer” would be a good name as well (bc that’s what it’s called in the exercise). Try saying “number of comparisons in linear in the height of the tree” instead of “running time is logarithmic” (a formulation where many things went wrong).

## Bad news

 $1\leq N\leq 300000$, so quadratic time is out of the question. This is what makes this a hard Kattis exercise.
Conclusion: Nike approach will not work.

## Understanding where elements to in BSTs

Let $k$ be a new key to be inserted.

First observation, after doodling for five minutes: The node containing $k$ will either be a right child of $\lfloor k\rfloor$ or a left child of $\lceil k\rceil$, the floor and ceiling (see def of Ordered ST). In particular, its depth will be 1 + the depth of those other nodes.

Best practice: Good idea to give things names when you start thinking about them. Let's use $l = \lfloor k\rfloor$ and  $h = \lfloor k\rfloor$ for “low” and “high”.

Good practice: It’s also super to think of concrete numbers. Let’s think of $k$ as 5 and of $l$ and $h$ has 3 and 8.

Second observation: Two different nodes $u$ and $v$ in any tree live in one of three different relationships: 

1. $u$ is an ancestor of $v$ 
2. $v$ is an ancestor of $u$ 
3. $u$ and  $v$  have a common ancestor

Draw these three cases(!)

Observe that for our concrete $l$ and $h$ in a BST, case 3 can never occur.
(Why? What would be the key stored at the common ancestor?)

```

 l      3
  \      \ 
   ...    11
   /      /
  h      8
```

or 

```
   h     8
  /     /
...   2
\      \
 l       3
```

So where does the new key k go? It's the left child of h or the right child of l, whichever is deeper.

**Conclusion**: Now we have it. (Add node 5 to the picture in both cases to see where it goes.) The cleanest way to write this down is that depth $d(k)$ of $k$ is the maximum of $\{0,d(l)+1, d(h)+1\}$, whenever these values are defined. (The first element accounts for the base case, when neither $l$ or $r$ even exist.)

## Implementing this

We now realise that the data type we need is an ordered symbol table on the set of keys, so that we can find floors and ceilings quickly. 
We also need another symbol table, not necessarily ordered, to look up the value of $d(l)$ and $d(h)$, and insert the value of $d(k)$.

Note: conceptually, the two symbol tables do something very different; one maintains a sequence of ordered keys under insertion (`put`) supporting `floor` and `ceiling`. We never care about the *values* that might be associated with a key. The other is a write-once (no value is ever updated) dictionary structure for $d$ supporting `put` and `get`. In particular, we care about the value associated with a key.

The ordered symbol table can be implemented with a Red-Black BST, and there aren't many other options. Let’s call it $S$.

* Note: $S$ is a search tree, but *not* the BST that we simulate.

On the other hand the unordered symbol table for $d$ can be implemented in many ways (including a Java `HashMap` or Python `dict`) – because unordered symbol tables are quite simple things. But for this exercise the easiest thing is to let $S$ serve two purpose at once: we can let $d(x)$ be the value stored in $S$ at key $x$. No extra data structure is actually needed.

Conceptual summary: Three things are in play:

1. the data structure (or “combinatorial structure”) that we try to simulate, a binary search tree. We never actually build it. We don’t care about it as a data type (no keys are mapped to values). It's just a combinatorial phenomenon that we want to simulate.

2. the ordered symbol table used to support `floor` and `ceiling` operations. This is a *data type*: We care about what its operations actually *do*, not so much how they do it. Our implementation happens to be the data *structure* Left-leaning Red-Black Search Tree. We don’t know any other implemenations.

* the unordered symbol table used to store $d(k)$. This is also a data type. We could implement this in a bunch of ways, a LLRB is fine, and we can re-used the same data structure as for point 2. By the way, at this point in the course, it’s the *only* efficient ST we know. Later there will be other data structures for unordered symbol tables (based on hashing).


## Terminology

We simulate a *combinatorial* structure ($T$, the BST) using an ordered symbol table, which is a *data type*.
Deliciously, our implementation of the data type happens itself to be a search tree, namely a red–black binary search tree $S$, but that is just an implementation detail.

* Do $S$ and $T$ have the same set of keys? (yes)

* Do $S$ and $T$ have the same structure? No. They have very different structures. The height of $S$ can be linear, the height of $T$ is guaranteed to be logarithmic.

