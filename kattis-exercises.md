Relevant Exercises on Kattis
============================

Here are some exercises relevant for the lectures on the Kattis server. 

Most of those come from past programming competitions, and some of them require you to pay a lot of attention to efficiency. (Of course, knowing which data structure trick you need to solve a particular exercise somewhat defeats the spirit of the task. Here, we just use them as an opportunity to apply what we just learned, instead of training problem solving.)


Lecture 4
---------

### Various queue data types

[guessthedatastructure](https://open.kattis.com/problems/guessthedatastructure) (strongly recommended)

### Priority queues, heaps

[numbertree](https://open.kattis.com/problems/numbertree) [pivot](https://open.kattis.com/problems/pivot),  [cookieselection](https://open.kattis.com/problems/cookieselection), [akcija](https://open.kattis.com/problems/akcija). All cute.

You can also do last week’s [ballotboxes](https://open.kattis.com/problems/ballotboxes) using a priority queue instead of binary search, but I can’t get my Python-solution accepted in time for the 4th last test case (no matter which Python version.)

### Fast Three-Sum

[sumsets](https://open.kattis.com/problems/sumsets) (hard, not necessarily recommended). Beware: very, very large instances, almost impossible in Python 3 (but doable in Python 2). Something similar (but easier and smaller) will appear as a programming exercise in the course in a few weeks. But if you want to train for a 5K race by running a marathon: go ahead.


Lecture 5
---------

### Sorting

[missinggnomes](https://open.kattis.com/problems/missinggnomes) (merging)

[veci](https://open.kattis.com/problems/veci), [shopaholic](https://open.kattis.com/problems/shopaholic), [busyschedule](https://open.kattis.com/problems/busyschedule), [sortofsorting](https://open.kattis.com/problems/sortofsorting),
[cups](https://open.kattis.com/problems/cups),
[lineup](https://open.kattis.com/problems/lineup), [sidewayssorting](https://open.kattis.com/problems/sidewayssorting), 
[classy](https://open.kattis.com/problems/classy), i
[musicyourway](https://open.kattis.com/problems/musicyourway)
[minimumscalar](https://open.kattis.com/problems/minimumscalar), [birds](https://open.kattis.com/problems/birds).

[airconditioned](https://open.kattis.com/problems/airconditioned) and  [delivery](https://open.kattis.com/problems/delivery) are very nice optimisation problems that happen to be solvable by the so-called “greedy” approach, which boils down to sorting, but the sorting part may not be the most interesting part of these questions.

Lecture 6
---------

### Symbol tables (dictionaries, associative arrays)

[grandpabernie](https://open.kattis.com/problems/grandpabernie), [baconeggsandspam](https://open.kattis.com/problems/baconeggsandspam). For these, you need a symbol table, some sort of list, and sorting. And it’s all straightforward – one can’t ask for more relevant exercise at this point in the course. You can use the inbuilt libraries from your favourite language, or the course libraries, depending on what you want to become more familiar with.

### Ordered symbol tables

[moviecollection](https://open.kattis.com/problems/moviecollection) is a  prototypical application of the API for ordered symbol tables. As far as I know, nothing in the Python or Java standard libraries is of any help here, so the Red-Black BST-based implementation from algs4 is your best bet. There is a somewhat different solution to this problem based on so-called *Fenwick Trees*, but they’re not part of the course.

#### Bst

Despite its inviting name, do *not* start by trying to solve [bst](https://open.kattis.com/problems/bst). It’s an absolutely great exercise, but quite nasty: If you merely simulate the behaviour of a binary search tree (which would be a perfectly nice exercise in its own right), you’ll run out of time for all but the smallest instances. (Think about it: hundreds of thousands of carefully chosen insertions into an unbalanced search tree. Quadratic time. *Boom.*)

So you have to do something else for the large instances; head-scratching is involved. 

Mind you, if you *do solve* bst, you have understood *everything* there is to understand about binary search trees, and about ordered symbol tables. 

For the brave pythonists: I’ve solved it in Python 2 using the RedBlackBST from algs4.searching (which works lik a charm). But I still needed at least one dirty trick to get past the 2 second time constraint, namely assembling the entire output as a list of strings and then printing all of it in one go using `print ('\n'.join(res))` instead of printing each line at a time. For Python 3, even that is not enough. I have some ideas, but not yet tried them, and they’d involve changing the Red Black BST from algs4 to include some extra pointers.

I like this exercise so much, I’ll try to put some input/output pairs on our own CodeJudge, so you can get slighly more useful feedback than what Kattis provides. I’m happy to discuss this exercise on the course Piazza instance.

It’s graded 6.9 on Kattis and took me over an hour (despite already having a red-black BST ready to copy-paste, and being in the middle of preparing the lecture for BST and therefore being quite familiar with the material), so you should expect *considerable* frustration. I guess the best language for this exercise is probably C++, because it has a very good ordered sequence data type as part of the standard library. (I haven’t tried, though.)


Lecture 7
---------

Some more really easy (unordered) symbol table exercises: [securedoors](https://open.kattis.com/problems/securedoors), [babelfish](https://open.kattis.com/problems/babelfish), [oddmanout](https://open.kattis.com/problems/oddmanout), [whatdoesthefoxsay](https://open.kattis.com/problems/whatdoesthefoxsay).
These can be solved using dictionaries based on hash tables; even Java’s HashMap or Python’s dict are perfect for this. But I could have posted these last week as well.

There are some quite esoteric exercises specifically about hash functions (instead of their applications to Symbol Tables) on Kattis: [enlarginghashtables](https://open.kattis.com/problems/enlarginghashtables), [hash](https://open.kattis.com/problems/hash), [hashing](https://open.kattis.com/problems/hashing). 
Of particular interest is [cuckoo](https://open.kattis.com/problems/cuckoo), because it concerns Cuckoo Hashing, the hashing scheme developed by ITU Professor Rasmus Pagh.
However these exercises are *hard* and I don’t recommend them in connection with this course.

Lecture 8
---------

Not much. All the sorting exercises from Lecture 5 remain relevant; there really isn’t anything new you can do with Quicksort that you couldn’t do before.

However, two of the mandatory programming assignments in the course should prepare you two Kattis exercises of high difficulty, not related to sorting.
[sumsets](https://open.kattis.com/problems/sumsets) is a (hard) version of *A5 Foursum*. If you’ve done the latter, attempt the former.
Similarly, [almostunionfind](https://open.kattis.com/problems/almostunionfind) is very similar to *A6 Disjoint Sets Move.*

Lecture 9
---------

There are a ton of “stringy” exercises on Kattis, but most of them are much harder than what we’d like. The one exception we’ve found is [bing](https://open.kattis.com/problems/bing), which is just perfect for this week’s lecture. (In Python 3, my first attempt got Time Limit Exceeded (the jargon is “it TLE’d”), but the same code goes through in the Python 2 without problems. I changed my Python 3 code to avoid objects, and it was accepted.) Unfortunately, there’s also a boring way of solving bing wihout using tries, which is almost as fast, and gets accepted.

Similarly, [phonelist](https://open.kattis.com/problems/phonelist) screams “please use a trie for me,” but the input size is so small that I was able to solve it using a much more pedestrian, sorting-based approach. Nice and easy exercise, but not for this lecture.

The other tasks  I’ve looked at (at least partially), require much more work.  I can’t really strongly recommend any of them, unless you want a challenge.

* [baza](https://open.kattis.com/problems/baza) looks super-relevant, but turns out to be harder than I thought. (I gave up when I realised what the problem was; maybe I'll get back to it.) 
* [autocorrect](https://open.kattis.com/problems/autocorrect) also needs a trie, but more tricks which we meet only later.
* [buzzwords](https://open.kattis.com/problems/buzzwords) Also hard.
* [dvaput](https://open.kattis.com/problems/dvaput) Looks realistic, but is not. (In particular, just sorting a suffix array will time out; there are probably some *very* long common prefixes in the input.)
* [suffixarrayconstruction](https://open.kattis.com/problems/suffixarrayreconstruction) Nominally about suffix arrays, but must be solved differently (I think.)
* [suffixsorting](https://open.kattis.com/problems/suffixsorting) Also very hard because of long overlapping substrings (I think). 


Lecture 10
----------

Graph connectivity: Start with [flyingsafely](https://open.kattis.com/problems/flyingsafely) and [wheresmyinternet](https://open.kattis.com/problems/wheresmyinternet).

Once you’re comfortable coding graph exploration, proceed with  [countingstars](https://open.kattis.com/problems/countingstars), [torn2pieces](https://open.kattis.com/problems/torn2pieces),  [coast](https://open.kattis.com/problems/coast), [10kindsofpeople](https://open.kattis.com/problems/10kindsofpeople) and [builddeps](https://open.kattis.com/problems/builddeps).

There are a ton of Kattis exercises about shortest paths.
Shortest paths in unweighted graphs are solved with BFS as well.
Start with [hidingplaces](https://open.kattis.com/problems/hidingplaces), then do [buttonbashing](https://open.kattis.com/problems/buttonbashing).

For weighted graphs, the canonical Dijkstra exercise is [shortestpaths1](https://open.kattis.com/problems/shortestpath1).  From there, a lot of shortest path exercises are very entertaining and boil down to finding a clever distance function, which can be challenging.  
Try [jailbreak](https://open.kattis.com/problems/jailbreak), [getshorty](https://open.kattis.com/problems/getshorty) and [resture](https://open.kattis.com/problems/treasure), in that order. Look at the difficulty rating of these exercises – if you can solve these, you should feel *very* good about yourself.  

Lecture 11
----------

The interactive [amazing](https://open.kattis.com/problems/amazing) is just about the best dfs exercise I’ve ever solved. Super satisfying. (To clarify: “ok indicates that there is door there and you may proceed in that direction to the neighboring cell” should maybe say “… and that you have proceeded in that direction to the neighbouring cell.”)

[reversingroads](https://open.kattis.com/problems/reversingroads)
