# Algorithms and data structures 
 
Lecture 10.00 – 11.45.

Welcome! 

Please be seated.

(no need to write this down, all of this text is available online, see below.)

---

# Today

* Course overview
* Break
* Chapter 1.5 in [Sedgewick–Wayne]

---

# Course overview

## The Algorithms and Data Structures “Course”

Three courses in one:

* Foundations of Computing: Algorithms and Data Structures (MSc Software Development)
* Algorithms and Data Structures (BSc Software Development)
* Algorithms and Data Structures (BSc Data Science)

All use the same 

* lectures 
* exercises 
* mandatory assignments 
* learnit instance: https://learnit.itu.dk/course/view.php?id=3018376
* exam 

---

## Exam

Four hours individual, (hand-)written exam on premises (*stedprøve*) with all written, non-digital aids.
 
You *can* bring the book in printed form, your notes, old exams with answers, a dictionary, etc.

You *cannot* use any digital devices or communication devices, such as a laptop, e-reader, mobile phone, smartwatch, etc.

Old exams (2011–2017) are on learnit. 2018 exams will appear soon. 

---

## Personnell

Two professors: 

- Thore Husfeldt (me)
- Riko Jacob

Welcome talk to us when you see us in the hallways.
Address us by first name.
Do *not* send us e-mail except on administrative matters (to Riko).

Huge group of student teaching assistants.
These are your main interface with the course.

Led by one Ph.D. Student

- Nina Mesing Stausholm
- See full list at learnit instance

---

## Natural languages

Official:

-   Data Science: English
-   BSc Software Development: Danish
-   MSc Software Development: English

Book, assignments, exercises, slides, lectures, and exam are in English.
You can answer in Danish (except for exam in English-language programmes).
Speak to me (or other teachers) in any language you like.

---

## Programming languages

- Default language for Data Science: (subset of) Python 
- Default language for Software Development: (subset of) Java
- Book is in (subset of) Java
- All Java code from the book is translated into Python at https://github.itu.dk/algorithms/AlgorithmsInPython
- You can pick either language if adventurous, but probably easiest to stick to the default.

Only write small (but complicated) programs.

What you need:

- Java or Python (3) environment from the command line
- An editor (VIM, Emacs, Atom, etc.)
- Some software libraries for the course


Strongly discouraged but not forbidden:

- Integrated Development Environment (Eclipse, IntelliJ, Jupyter, etc.)

Sources of frustration:

- Many different backgrounds
- Many different “first courses”
- Expected to manage your own machine
- Transitionn from first ever programming class (“The world is like *this*”) to their second ever programming class (“The world can also be like *this*”)

Don’t despair, we’re here for you.

---

## Learning Activities

Weekly **lecture**

* Monday, 10–12.
* Lecture schedule on learnit.
* Read book section in advance or not? Matter of personal taste. 

Weekly **exercise session**

* Wednesday, Thursday, or Friday, 2 hours
* Lab sessions (time spent on solving exercises, TA in room available for help): Wednesdays
* Presentation sessions (time spent on discussing solutions at blackboard). Wednesdays, Thursdays, and Fridays.
* Dozen exercises each week, mostly from the book, see learnit schedule
* Sign up for class at learnit instance, opens tonight (Monday 20:30).

10 **mandatory assignments** on learnit

* Deadline most Mondays at 9:55.
* Graded pass/fail
* Must pass 7/10 (no other effect on final grade)
* Mostly hand in in teams of at most 3: https://learnit.itu.dk/mod/choicegroup/view.php?id=80933
* Exception: Assignment 1 is mandatory and individual.
* Most programming assignments supported by Code Judge automatic grading system (linked from learnit)
* Done them before? (Re-taking the course.) Don't do them again, you're golden.

Office hours with Nina (4B10), Wednesdays 14-16, focus on theory.


**Piazza** instance at https://piazza.com/class/jr90sx0xoyy5vr . Ask questions there.

---

##  Materials

Course **book**.

* [SW] Robert Sedgewick and Kevin Wayne, *Algorithms*, 4th edition. 
* Lectures and most weekly exercises are from book.
* We cover roughly 1.3–5, 2, 3, 4, 5.1–2.
* Booksite: https://algs4.cs.princeton.edu/home/
* Summaries for each section at booksite.
* Thore’s quick Danish translations: http://www.itu.dk/people/thore/ads/13stacks-da.html
* Slides for book (at booksite)
* Even online course at coursera.org (videos featuring Kevin instead of lectures featuring Thore)
* Kindle edition exists, but suboptimal (not for exam!)

Other online materials linked from learnit, such as:

* Old exams from 2011–2017
* Repository with *all* mandatory programming exercises: https://bitbucket.org/rikj/bads-labs/src/master/
  - Available as zipped PDF downloads
  - Best practice: clone the entire repo to your own machine (requires git and latex installed)
  - Each exercise may be updated before it goes “official”
* Sometimes lecture summaries https://github.itu.dk/algorithms/ads-2019-notes/ (like this one).

---

## Good Use of Your Time

Typical week:

* 3h: Work on weekly **exercises**. Great group activity.
	 Suggest scheduling this (“meet every Wednesday 13-16 with Linda and Børge for AD exercises”)
* 2h: **Read** course book. Alone, at home, pen and paper close by, cat in lap, cuppa coffee.
* 2h: Participate in exercise session.
* 2–5h: Work on **mandatory assignment**. In group, maybe same group as exercises.
* 2h: Attend Monday **lecture**. Group of 300. 

Unstructured but mildly useful: Visit study lab, chat w/ other students over coffee or beer, surf internet for other sources (Wikipedia, lecture notes from similar courses, stackoverflow questions, YouTube videos, …).
In particular, 

* Bob Swedgewick’s videos for his Princeton course.
* Coding exercises on Kattis. 
* Extremely concise alternative book: Mehlhorn & Sanders, free at https://people.mpi-inf.mpg.de/~mehlhorn/Toolbox.html .

Note:

* Attending lecture (or watching videos) is entertaining, easy, and quite useless.
* Weekly exercise preparation is frustrating, hard, and useful.
* This is a theory course, programming is supplementary. 

Only mandatory part: mandatory assignment.

---

## Rules of Behaviour 

Please…

* Be on time (i.e., *ahead* of time)
* Move to the middle of the aisle
* Close your laptops (!)
* Do not record the lecture (nor the exercise sessions)
* If you have to use a laptop: Use the back row.

---

# Union–Find

[SW] Chapter 1.5.

## Tiny UF input

From https://algs4.cs.princeton.edu/15uf/tinyUF.txt

```
10
4 3
3 8
6 5
9 4
2 1
8 9
5 0
7 2
6 1
1 0
6 7
```

## Models

* Network
* Equivalence classes
* Disjoint sets

`9 4` means:

* Connect the component of _site_ 9 with the component of 4 by an _edge_ between 4 and 9.
* Add the _relation_ \\(9\sim 4\\). 
  This ensures \\( [9]  = [4] \\) .
* _Union_ the _set_ containing the _element_ 9 with the set containing the element 4.

⚠︎: “Sharing an edge” and “being connected” are not the same.
 “`1 2`” adds an _edge_ between nodes 1 and 2.
“`2 3`” adds an _edge_ between nodes 2 and 3.
Node 1 and 3 are _connected_, but share no edge.


## Naive solution (Python)

```python
import sys

n = int(sys.stdin.readline())
S = [set([p]) for p in range(n)] # S[0]={0}, S[1]={1}, ...

for line in sys.stdin.readlines():
    p, q = map(int,line.split())  # 4 , 7
    if S[p] == S[q]: continue # Same set? Then skip.
    print p, q # Different sets, say {1,4,8} and {3,7}
    U = S[p].union(S[q])  # {1,3,4,7,8}
    for x in U: S[x] = U
```

## Naive solution (Java)

```java
import java.io.*;
import java.util.*;

public class UFNaive
{
    public static void main(String[] args)
    {
	Scanner in = new Scanner(System.in);
	int n = in.nextInt();
	List<Set<Integer>> C = new ArrayList<Set<Integer>>(n);
      # List<Set<Integer>> C = new ArrayList();
	for (int i= 0; i<n; i+= 1)
	    C.add(new TreeSet<Integer>(Collections.singleton(new Integer(i))));
          # C.add(Set.of(i));
	while (in.hasNextInt())
	{
	    int p = in.nextInt();
	    int q = in.nextInt();
	    if (C.get(p).containsAll(C.get(q))) continue;
          # if (C.get(p)==C.get(q))
	    System.out.println (p + " " + q);
	    Set<Integer> S = new TreeSet<Integer>(C.get(p));
	    S.addAll(C.get(q)); 
	    for (int x: S) C.set(x, S);
	}
    }
}
```

## Disjoint Sets I

Basically same task as solving Disjoint Sets I. 
- Solve that exercise yourself
- Watch us do it at the videos “Best practices for solving “Disjoint Sets I”” https://learnit.itu.dk/mod/page/view.php?id=96155

## Various Union–Find Data Structures

Kevin’s slides for [SW] 1.5.

