# Lecture 10

Graphs (directed, undirected, weighted), BFS, shortest path

Have a seat -- close your laptop if anybody else can see your screen.

Remember course evaluations starting today

You can find this document at https://github.itu.dk/algorithms/ads-2019-notes/blob/master/lecture-10.md

----
## Book Chapters and Kevin's slides
- 4.1 undirected graphs [slides](https://algs4.cs.princeton.edu/lectures/41UndirectedGraphs.pdf)
[(2x2)](https://algs4.cs.princeton.edu/lectures/41UndirectedGraphs-2x2.pdf)
- 4.2  directed graphs [slides](https://algs4.cs.princeton.edu/lectures/42DirectedGraphs.pdf)
[(2x2)](https://algs4.cs.princeton.edu/lectures/42DirectedGraphs-2x2.pdf)
- 4.3  (MST) weighted graphs [slides](https://algs4.cs.princeton.edu/lectures/43MinimumSpanningTrees.pdf)
[(2x2)](https://algs4.cs.princeton.edu/lectures/44ShortestPaths-2x2.pdf)
- 4.4 shortest path [slides](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf)
[(2x2)](https://algs4.cs.princeton.edu/lectures/44ShortestPaths-2x2.pdf)

----
## Plan for today
- Application Route Planning - weighted?
- Graphs (directed, undirected)
- BFS
- Dijkstra's Algorithm

-----
## Route Planning
[google maps](https://www.google.de/maps/dir/55.6842792,12.5700364/IT-Universitetet+i+K%C3%B8benhavn,+Rued+Langgaards+Vej,+K%C3%B8benhavn/@55.6705049,12.5630367,14z/data=!4m9!4m8!1m0!1m5!1m1!1s0x46525345017412b3:0xa280a05485937e38!2m2!1d12.590958!2d55.659635!3e1)

[best connectivity](https://algs4.cs.princeton.edu/lectures/15UnionFind.pdf#page=5)

#### Algorithmic Ideas:
- rumor spreading
- fire
- infection
- wavefront 
- ...

Now we have an application and an algorithmic idea that want to be connected by some modeling and API: Graphs

---
## Graphs
[SW 4.1 p518, 4.2 p566] 
[Digraph](https://algs4.cs.princeton.edu/lectures/42DirectedGraphs.pdf#page=3)

- Set of **Vertices**, in algs4 the numbers {0,...,n-1}
- Collection of **Edges**, i.e., pairs of vertices
- **directed edges**: we distinguish (2,7) from (7,2)
- **undirected graphs**: only insert anti-parallel edges, i.e. (2,7) and (7,2)
- antiparallel and parallel edges and self-loops are allowed

---
## Path
sequence of vertices that are connected by edges
- no repeated edges
- **simple**: no vertices repeated
- **cycle**: first and last vertex are the same
- **length**: number of edges on the path
- graph G is **connected** if there is a path for all pairs
- ow there are **connected components**

---
## API / data type for graphs in algs4
[SW 4.1 p522, 4.2 p568] 
- **Vertices** are numbers {0,...,n-1}
- **Edges** specified by pair
- **Neighbors**, adjacent nodes,  accessible via Iterable at node

### Representation Alternatives 
[Digraph Representations](https://algs4.cs.princeton.edu/lectures/42DirectedGraphs.pdf#page=17)

--- 
## BFS
[code](https://algs4.cs.princeton.edu/lectures/41UndirectedGraphs.pdf#page=47)

---
---
## Shortest Path
- graph with weights on the edges [weighted graph API](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf#page=7)

- minimal sum of weights

Integer weights could be solved with BFS in a bigger graph

Idea: Simulate the mechanical machine!

---
## The Solution is a Shortest Paths Tree

We have seen trees as data structures throughout.

Now we generalize and see graphs as input (and as data structures)



---
## Shortest Path [SP API](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf#page=12)

edge objects with 
- **from()** (tail)
- **to()** (head)
- **weight()**

shortest path object
- initialized on Graph and source vertex
- **distTo(int v)**
- **pathTo(int v)**


---
### Algorithmic [Framework](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf#page=14)

works for Dijkstra's Algorithm, [DAG Algorithm](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf#page=35) and [Bellman-Ford Algorithm](https://algs4.cs.princeton.edu/lectures/44ShortestPaths.pdf#page=54)

[Intro DAG](https://algs4.cs.princeton.edu/lectures/42DirectedGraphs.pdf#page=40)

---
## Outlook
- concrete graphs, for example road networks, have lots of structure to exploit
- you can do a lot by creating the right graph
- variants: public transport passenger information
- strong example of Algorithms Engineering
