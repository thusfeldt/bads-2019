# Lecture 11

Brief advertisemend for Will Code For Drinks.

---

Next lectures:

* Memory hierarchies, machine models, B-trees (MSc)

* String searching (BSc)

* Nina Stausholm on union--find in \\(O(log^* n)\\)  (on a Thursday)

* Exam prep

---

Union--find--move debriefing maybe as a video?

Assignments 10 and 11 now online

Quizzes on Learnit


---

Lecture:

DFS (including applications for top. sort and strongly connected  components.) [SW 4.2]

Minimum Spanning trees [SW 4.3]

---

Final “big-picture” overview on the blackboard:

Four of our algorithms follow the idea of iteratively growing a set S of “visited” vertices, following single edges that point out of S.

These four algorithms differ in *which* of the many edges they choose.

* Depth first search picks *some* edge of the most recently added edges. (“The youngest” of the edges pointing out of S.)


* Breadth first search picks *some* edge of the least recently added edges. (“The oldest” of the edges pointing out of S.)

* Prim’s algorithm picks the lightest edge.

* Dijsktra’s algorithm picks the edge that minimises the sum of the edge lenght and the distance to the endpoint in S.
