# Lecture 2

Arrays, lists, and collections

Have a seat -- close your laptop if anybody else can see your screen

You can find this document at https://github.itu.dk/algorithms/ads-2019-notes/blob/master/lecture-2.md

[Kevin's slides](https://algs4.cs.princeton.edu/lectures/13StacksAndQueues.pdf)
[slides 2x2](https://algs4.cs.princeton.edu/lectures/13StacksAndQueues-2x2.pdf)
## Plan for today

* About the course: lectures, assignments, exercises
* Investigating and understanding off-the-shelf lists
* Using stacks to evaluate arithmetic expressions
* Implementing stacks as linked lists
* Implementing stacks as dynamic arrays
* Implementing queues
* RandomQueue assignment: Iterators and randomness

---

## About the course

Two professors: 

- Thore Husfeldt 
- Riko Jacob (me)

Topic of the course:

_Efficient_ implementation of basic data structures

independent of java or python.  
Implementations are only for precision / illustration -- use only black-boxes that we already covered in the course.

### lectures on video

Take a look at [Bob Sedgewick lecturing at youtube](https://www.youtube.com/playlist?list=PLeipq3NgyTAl20PEUKBXMsoOCE6Q_vkp1)

Relevant for today's lecture (all relating to Chapter 1.3 in the [book](https://algs4.cs.princeton.edu/home/)):

| name               |  time | content                                                           |
|--------------------|------:|-------------------------------------------------------------------|
| 'algs4partI L3 S1' | 16:25 | 1.3 stacks: API, implementation as linked list / fixed size array |
| 'algs4partI L3 S2' |  9:57 | 1.3 resizing array                                                |
| 'algs4partI L3 S3' |  4:34 | 1.3 queues: API and implementation                                |
| 'algs4partI L3 S4' |  9:27 | 1.3 generics (type parameters, java only)                         |
| 'algs4partI L3 S5' |  7:17 | 1.3 iterators                                                     |
| 'algs4partI L3 S6' | 13:26 | 1.3 applications: ... arithmetic evaluation ...                   |

(roughly 1 hour)

### our lectures here

something else:
- active interaction
- tangible models
- stories
- ...

### Assignments 

Each Monday before the lecture, an assignment is due

NEW: some of the assignments are theoretical (not coding in java or python)
- one week to finish
- will be discussed in exercise class in the week of due date
- first one: 'catenablequeue' on bads-labs 

There will be a activity on learnIT that allows you create the group in which you hand in the assignments.
If you participated in the course before and successfully completed the required assignments, you don't need to do them again.  
In this case write email to `rikj@itu.dk`.

### needed for RandomQueue
https://bitbucket.org/rikj/bads-labs

java generic types in 'Introductory Programming', or in the book (Chapter 1.3).
(there are no generic types in python)

[Kevin’s slides for 1.3] 33–41 (Generics in Java, Java students only)

#### Iterators 
[Kevin’s slides for 1.3] 42–47 (Iterators)
#### Loitering
[Kevin’s slides for 1.3] 15
#### Randomness 

```
public Iterator<Item> iterator() // return an iterator over the items in random order
```

##### Directly from the library:
https://docs.python.org/3/library/random.html#random.shuffle

https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html#shuffle-java.util.List-

##### Some additional considerations

Presumably the underlying algorithm:
https://en.wikipedia.org/wiki/Random_permutation#Knuth_shuffles

For an array of size 100, and using the above pseudo-random number generators, is every single permutation possible as output?

---

## 'off the shelf' lists

### Python List Mystery

###### ListMystery.py
```python
L = []
for i in range (100_000):
    L.insert(i,0)
```

###### ListMystery2.py
```python
L = []
for i in range (100_000):
    L.insert(0,0)
```

### Java List Mystery

###### ListMystery.java
```java
import java.util.*;
public class ListMystery
{
    public static void main(String[] args)
    {
        List<Integer> L = new ArrayList<Integer>();
        for (int i = 0; i< 500_000; ++i) L.add(i,0);
    }
}
```

###### ListMystery2.java
```java
import java.util.*;
public class ListMystery2
{
    public static void main(String[] args)
    {
        List<Integer> L = new ArrayList<Integer>();
        for (int i = 0; i< 500_000; ++i) L.add(0,0);
    }
}
```

---

## Towards an Analysis of Data Structures

What does the running time depend on?

* operation (OK)
* choice of data structure (OK)
* size of data structure (OK)
* the values of the parameter??? 

https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html
https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html#add-int-E-

---

## Array / Fixed-Length List Data Type

We start by using only an 'array' with fixed length and constant access time

| Python | Java | Meaning |
|--------|------|----------|
| `L= [None] * N`   | `String[] L = new String[N];` | list of N elements |
| `L[i]= j`  | `L[i]= j` | `i`th item is now `j`, rest untouched |


---

## API Stacks

[Kevin’s slides for 1.3] 1–5

## Expression Evaluation
[Kevin’s slides for 1.3] 55–59 (Expression evaluation)

## Stacks/Queues as Linked Lists / Dynamic Arrays
[Kevin’s slides for 1.3] 1–32 (Stacks, Queues, as Linked Lists and Dynamic Arrays)

How can there be loitering for the linked list implementation of the FIFO queue?

