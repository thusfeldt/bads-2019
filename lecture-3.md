# Lecture 3

Analysis of Algorithms [SW, 1.4]

10:00 – 12:00

Have a seat. Close your laptops.

---

# Reasoning about algorithms

* Three levels of abstraction
* All introduced in a1 and a2
  - Abstract datatype given by API / interface. Example: FIFO queue.
  - Implementation as a data structure using only bounded arrays / fixed-length lists and constant-size objects. Example: Linked list, unbounded / resizing / dynamic array.
  - Running code in a concrete programming language
* Cognitive challenge: Awareness of level of abstraction
* Translation between well-defined layers of abstraction = main virtue of computational thinking 

# Assignment schedule 

* Today: deadline for a2 (Catenable Queues, theory)
* Next monday:  deadline for a3 (Random Queue, programming)
* (No new assignment posted today)

---

# Intermezzo: Bounded iteration

Want do do_something(i) for $i \in \\{j,\ldots, n-1\\}$.

``` python
i = j
while (i < n):
    do_something(i)
    i = i + 1
```

``` java
int i = j;
while (i < n) {
    do_something(i);
    i = i + 1;
}
```

Common enough to have idiomoatic expressions for this in both languages. Somewhat confusing for this course, but feel free to use them in your code:

``` python
for i in range(j, n):
    do_something(i)
```

``` java
for (int i = j; i < n; ++i)
    do_something(i);
```



#  Three-Sum

(Live coding ```ThreeSum.java```, mixed with Kevin Wayne‘s slides))

###### ThreeSum.java
```
import edu.princeton.cs.algs4.*;
import java.util.*;


public class ThreeSum
{
    static int count(int[] a)
    {
	int ct = 0;
	for (int i = 0; i < a.length; ++i)
	    for (int j = i+1; j < a.length; ++j)
		for (int k = j+1; k < a.length; ++k)
		    if (a[i]+a[j]+a[k] == 0) ++ct;
	return ct;
    }

   static int[] randArray(int N)
    {
        Random R = new Random();
        int[] a = new int[N];
        for (int i = 0; i<N; ++i) a[i] = R.nextInt();
        return a;
    }

   public static void main(String[] args)
   {
       int N = Integer.parseInt(args[0]);
       int a[] = randArray(N);
       // Next line runs count a number of times without measuring it,
       // for various annoying reasons (just-in-time compilation,
       // cache lines, processor allocation).
       // After some experimentation, 3 times was good enough
       // on Thore's Mac in February 2018. Other machines 
       // may behave differently. Black art.
       for (int i= 0; i< 3; ++i) count(a); 

       // Now we're ready for a single execution of count()
       // whose running time we measure: 
       Stopwatch S = new Stopwatch();
       StdOut.println(count(a));
       StdOut.println(S.elapsedTime());
   }
}
```
###### threesum.py

```python
import sys
from algs4.stdlib import stdio, stdrandom

def count(a):
    ct = 0
    N = len(a)

    for i in range(N):
        for j in range(i+1, N):
            for k in range(j+1, N):
                # 0 <= i < j < k < N
                if a[i] + a[j] + a[k] == 0: ct+= 1
    return ct

if len(sys.argv) == 1:
    N = int(stdio.readInt())
    a = [None] * N
    for i in range(N):
        a[i] = stdio.readInt()
else:
    N = int(sys.argv[1])
    a = [None] * N
    for i in range(N):
        a[i] = stdrandom.uniformInt(-N,N)

print(count(a))
```
---


# Binary Search, Better Three-Sum

(On the blackboard. Follows Kevin Wayne’s slides.)

---

# Some Memory details

Kevin’s slides for Java, but also some figures from [website for Sedgewick & Wayne, Programming in Python, sec. 4.1] 
(https://introcs.cs.princeton.edu/python/41analysis/) for basic Python data structures.

