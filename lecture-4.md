# Priority Queues

Sedgewick–Wayne pp. 189-191, 244–247, section 2.4

* Comparing stuff
* Priority queue
* Heaps
* Heapsort
* Three-Sum Fast

---

# I mean it

Please close your laptops (or sit at the back row)

---

# Comparing stuff

###### comparedemo.py
```python
class Date:
    def __init__(self, d, m, y):
        self.day = d
        self.month = m
        self.year = y

    def __lt__(self, that):
        if (self.year < that.year): return True
        if (self.year > that.year): return False
        if (self.month < that.month): return True
        if (self.month > that.month): return False
        if (self.day < that.day): return True
        return False


# a = 5
# b = 4

# a = "Åbenrå"
# b = "Aachen"

a = Date(3, 5, 10)
b = Date(4, 5, 1)

if (a.__lt__(b)): print("a < b")
else: print("a >= b")

# syntactic sugar:
# if (a < b): print("a < b")
```

# Java's comparable

Kevin's slides for Java's Comparable, Section 2.1, slides 1–11
 
# Priority queues, heaps, heapsort

Some blackboard work.

Not shown in class, but here's the Python code for the naive implementation of the priority queue data type:

###### unsortedpq.py
```python
class MaxPQ:
    def __init__(self):
        self.pq = [None] * 10 # I burn in Hell
        self.n = 0


    def is_empty(self):
        return self.n==0

    def insert(self,item):
        self.pq[self.n] = item
        self.n += 1
        print (self.pq)

    def delMax(self):
        if self.is_empty(): raise NoSuchElementException()
        curmax = 0
        for i in range(self.n):
            if self.pq[curmax] < self.pq[i]: 
                curmax = i
        self.pq[self.n-1], self.pq[curmax] = self.pq[curmax], self.pq[self.n-1]
        self.n-= 1
        return self.pq[self.n]
    
pq = MaxPQ()
pq.insert('P')
pq.insert('Q')
pq.insert('E')
print(pq.delMax())
pq.insert('X')
pq.insert('A')
pq.insert('M')
print(pq.delMax())
pq.insert('P')
pq.insert('L')
pq.insert('E')
print(pq.delMax())
```

# Heap-based PQs, Heapsort

Blackboard work with paper and magnets corresponding to Kevin's slides for section for Section 2.4.


# ThreeSum revisited

ThreeSumFast, using sorting and binary search. See SW, pp. 189–191.





