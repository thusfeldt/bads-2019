Links to Bob Sedgewick’s Videos
===============================

 [Bob Sedgewick lecturing at youtube](https://www.youtube.com/playlist?list=PLeipq3NgyTAl20PEUKBXMsoOCE6Q_vkp1)

Lecture 2
---------

| name               |  time | content                                                           |
|--------------------|------:|-------------------------------------------------------------------|
| 'algs4partI L3 S1' | 16:25 | 1.3 stacks: API, implementation as linked list / fixed size array |
| 'algs4partI L3 S2' |  9:57 | 1.3 resizing array                                                |
| 'algs4partI L3 S3' |  4:34 | 1.3 queues: API and implementation                                |
| 'algs4partI L3 S4' |  9:27 | 1.3 generics (type parameters, java only)                         |
| 'algs4partI L3 S5' |  7:17 | 1.3 iterators                                                     |
| 'algs4partI L3 S6' | 13:26 | 1.3 applications: ... arithmetic evaluation ...                   |

Lecture 3
---------

| name               |  time | content                                                           |
|--------------------|------:|-------------------------------------------------------------------|
| ['algs4partI L2 S1'](https://youtu.be/qwcVeId9RqQ) | 8:16 | 1.4 introduction |
| ['algs4partI L2 S2'](https://youtu.be/tJYBmtjsIkA) | 12:48 | 1.4 mathematical models |
| ['algs4partI L2 S3'](https://youtu.be/-RsX2bARq-0) | 11:35 | 1.4 order of growth classification |
| ['algs4partI L2 S4'](https://youtu.be/_wArQr-dzoE) | 11:35 | 1.4 theory |
| ['algs4partI L2 S5'](https://youtu.be/whcAZ7lSTJw) |  8:11 | 1.4 memory                                |

Lecture 4
---------

| name               |  time | content                                                           |
|--------------------|------:|-------------------------------------------------------------------|
| ['algs4partI L4 S1'](https://youtu.be/GENxxQx9Erg) |  | 2.1 rules of the game |
| ['algs4partI L7 S1'](https://youtu.be/L8fNRwqR60o) |  | 2.5 priority queues intro |
| ['algs4partI L7 S2'](https://youtu.be/Dzmq297giJ0) |  | 2.5 binary heaps |
| ['algs4partI L7 S3'](https://youtu.be/lfHUpPGEgAM) |  | 2.5 heapsort |
| ['algs4partI L2 S3'](https://youtu.be/-RsX2bARq-0) | | 1.4 last 3 minutes, about fast 2-sum and 3-sum |

After this it makes sense to see one of the coolest applications of priority queues in action: event-driven simulations:

| name               |  time | content                                                           |
|--------------------|------:|-------------------------------------------------------------------|
| ['algs4partI L7 S4'](https://youtu.be/gO_VkLIJCmw) |  | 2.1 event driven simulation |

Lecture 5
---------

| name | time |content |
|------|-------:|---|
| ['algs4partI L4 S1'](https://youtu.be/GENxxQx9Erg) |  | 2.1 rules of the game |
| ['algs4partI L4 S2'](https://youtu.be/0nNqHW1eFUQ) | | 2.1 selection sort |
| ['algs4partI L4 S3'](https://youtu.be/ou6MimDWZ7w) | | 2.1 insertion sort |
| ['algs4partI L5 S1'](https://youtu.be/ypae0cmi7hM) || 2.2 merge sort |
| ['algs4partI L5 S2'](https://youtu.be/WVl2QSe4R14) || 2.2 merge sort (bottom up) |
| ['algs4partI L5 S4'](https://youtu.be/Dpq1Mt1DS4w)|| 2.2 comparators |
| ['algs4partI L5 S5'](https://youtu.be/YGawSKpwHbw) || 2.2 stability |

Lecture 6
---------

|name|time|content|
|-|-:|-|
| ['algs4partI L8 S1'](https://youtu.be/g8DejJF9THI) || 3.1 Symbol tables |
| ['algs4partI L8 S2'](https://youtu.be/M0GlIQwN1hY) || 3.1 Elementary implementations |
| ['algs4partI L8 S3'](https://youtu.be/6Ef9yUfvBJQ) || 3.1 Ordered operations | 
| ['algs4partI L8 S4'](https://youtu.be/vWchQ0Di7yM) || 3.2 Binary search trees  |
| ['algs4partI L8 S5'](https://youtu.be/wk6VNdY3pnk) || 3.2 Ordered operations  |
| ['algs4partI L8 S6'](https://youtu.be/6zoBvuPk510) || 3.2 Deletion  |
| ['algs4partI L9 S1'](https://youtu.be/N-yla7zw0Fw) || 3.3 Balanced search trees: 2-3| 
| ['algs4partI L9 S2'](https://youtu.be/8HVMaEqQJDU) || 3.3 Balanced search trees: Red–Black| 

Lecture 7
---------

Hash tables:

|name|time|content|
|-|-:|-|
| ['algs4partI L10 S1b'](https://youtu.be/EWX8bVSlgx0) || 3.4 Hash tables |
| ['algs4partI L8 S2'](https://youtu.be/B7O-Mj3IxYw) || 3.4 Separate chaining |
| ['algs4partI L8 S3'](https://youtu.be/EFdYMu2MsYo) || 3.4 Linear probing | 
| ['algs4partI L8 S3'](https://youtu.be/lGT7bjc7Tdc) || 3.4 Context (applications, stories, details) | 

Analysis. We’re deviating from Sedgewick–Wayne because we pay more attention to the Landay notation (“big-Oh”), and take amortised analysis much more serious. Thus, no cosy videos with Uncle Bob for this part. But you can revisit his description resizing arrays, where he sketches the argument in connection insertions-only stacks:

|name|time|content|
|-|-:|-|
| ['algs4partI L8 S4'](https://youtu.be/vWchQ0Di7yM) || 2.3 Amortised analsysis of resizing |



Lecture 8
---------

|name|time|content|
|-|-:|-|
| ['algs4partI L6 S1](https://youtu.be/1a-chpO4bgQ) || 2.3 Quicksort |
| ['algs4partI L5 S3](https://youtu.be/iK6HLS6SN2Q) || 2.2. Complexity of sorting |


Lecture 10
----------
|name|time|content|
|-|-:|-|
| ['algs4partII L01 S1'](https://www.youtube.com/watch?v=4jJpMlEtOqo&index=56&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX) || 4.1 Undirected Graphs Intro |
| ['algs4partII L01 S2'](https://www.youtube.com/watch?v=aAWGLOrmpCM&index=57&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX) || 4.1 Undirected Graphs API   |
| ['algs4partII L01 S2'](https://www.youtube.com/watch?v=lI9fgDPNcRs&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=59) || 4.1 BFS |
| ['algs4partII L02 S1'](https://www.youtube.com/watch?v=2_FiizyHq7o&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=62) || 4.2 Directed Graphs Intro |
| ['algs4partII L02 S2'](https://www.youtube.com/watch?v=10oIuevrYfo&index=63&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX) || 4.2 Digraph API |
| ['algs4partII L04 S1'](https://www.youtube.com/watch?v=gcPhkSpO2mk&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=73) || 4.4 Shortest Path Intro |
| ['algs4partII L04 S2'](https://www.youtube.com/watch?v=uAb1Z5P9zfA&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=74) || 4.4 SP Properties |
| ['algs4partII L04 S3'](https://www.youtube.com/watch?v=uzHJXbToiIU&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=75) || 4.4 Dijkstras Algorithm|
| ['algs4partII L04 S4'](https://www.youtube.com/watch?v=Qp9zy9qMJzE&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX&index=76) || 4.4 edge weighted DAGs|
| ['algs4partII L04 S5'](https://www.youtube.com/watch?v=A54rUI6CPSs&index=77&list=PLRdD1c6QbAqJn0606RlOR6T3yUqFWKwmX) || 4.4 negative weights |
